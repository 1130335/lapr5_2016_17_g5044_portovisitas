:- dynamic poi/4, road/4.


%predicado link
%
%Determina ligacao entre poi
%link(A,B,C,D).
%
%A: Id POI Origem
%B: Id POI Destino
%C: Distancia M
%D: Inclinacao estrada? true or false
link(A,B,C,D):- road(A,B,C,D).
link(A,B,C,D):- road(B,A,C,D).

%predicado getListPOI
%
%Gera lista de pois de acordo com base de conhecimento
%getListPOI(A).
%
%A: Id POI Origem
listPOI(L,Lf):-
	poi(Id,Location,LWH,D),
	\+ member(poi(Id,Location,LWH,D),L),
	listPOI([poi(Id,Location,LWH,D)|L],Lf).
listPOI(Ltemp,L):- reverse(Ltemp,L).

getListPOI(L):-
	listPOI([],L),
	!.

%predicado getPOIByLocation
%
%Retorna id de poi atraves do local
%getPOIByLocation(A,B).
%
%A: Nome local POI
%B: POI Id 
getPOIByLocation(Name,Id):-
	poi(Id,Name,_,_),
	!.

%predicado getListPath
%
%Retorna caminho entre POIStart e POIEnd
%getListPath(A,B,C,D).
%
%A: POI Start
%B: POI End
%C: Considera restricoes de acessibilidade?
%D: Lista resultado
getListPathDet(POIStart,POIEnd,Restriction,LTemp,Lf):-
	Restriction == true,
	link(POIStart,POIEnd,Distance,false),
	poi(POIEnd,_,_,Duration),
	append([],[POIStart,POIEnd,false,Distance,Duration],Elem),
	append([Elem],LTemp,Lf).
getListPathDet(POIStart,POIEnd,_,LTemp,Lf):-
	link(POIStart,POIEnd,Distance,R),
	poi(POIEnd,_,_,Duration),
	append([],[POIStart,POIEnd,R,Distance,Duration],Elem),
	append([Elem],LTemp,Lf).
getListPathDet(POIStart,POIEnd,Restriction,LTemp,Lf):-
	Restriction == true,
	link(POIStart,POIMiddle,Distance,false),
	poi(POIMiddle,_,_,Duration),
	append([],[POIStart,POIMiddle,false,Distance,Duration],Elem),
	\+ member(Elem,LTemp), !,
	getListPathDet(POIMiddle,POIEnd,Restriction,[Elem|LTemp],Lf).
getListPathDet(POIStart,POIEnd,Restriction,LTemp,Lf):-
	link(POIStart,POIMiddle,Distance,R),
	poi(POIMiddle,_,_,Duration),
	append([],[POIStart,POIMiddle,R,Distance,Duration],Elem),
	\+ member(Elem,LTemp), !,
	getListPathDet(POIMiddle,POIEnd,Restriction,[Elem|LTemp],Lf).

getListPath(POIStart,POIEnd,Restriction,L):-
	getListPathDet(POIStart,POIEnd,Restriction,[],LTemp),
	reverse(LTemp,L).

%predicado getTotalDistancePath
%
%Calcula distancia total entre pois
%getTotalDistancePath(A,B).
%
%A: Caminho entre POIs
%B: Distancia total
getTotalDistancePath([],0).
getTotalDistancePath([[_,_,_,Distance,_]|Ls],Total):-
	getTotalDistancePath(Ls,Total2),
	Total is Distance + Total2.

%predicado getListPathWTD
%
%Gera lista com caminho e distancia total
%getListPathWTD(A,B).
%
%A: Lista sem distancia total
%B: Lista com distancia total
getListPathWTD(L,Lf):-
	getTotalDistancePath(L,Total),
	append([L],[Total],Lf).

%predicado getAllListPathWTD
%
%Gera lista com todos os caminhos entre POIStart e POIEnd
%incluindo distancia total
%getAllListPathWTD(A,B,C,D).
%
%A: POI Start
%B: POI End
%C: Considera restricoes de acessibilidade?
%D: Lista resultado
getAllListPathWTDDet(POIStart,POIEnd,Restriction,LTemp,Lf):-
	getListPath(POIStart,POIEnd,Restriction,LPath),
	getListPathWTD(LPath,LPathWTD),
	\+ member(LPathWTD,LTemp), !,
	getAllListPathWTDDet(POIStart,POIEnd,Restriction,[LPathWTD|LTemp],Lf).
getAllListPathWTDDet(_,_,_,LTemp,Lf):-
	reverse(LTemp,Lf).

getAllListPathWTD(POIStart,POIEnd,Restriction,L):-
	getAllListPathWTDDet(POIStart,POIEnd,Restriction,[],L),
	!.

%predicado getMinorPath
%
% Retorna caminho mais curto
getMinorPathDet([],Minor,Minor).
getMinorPathDet([Elem|Ls],Minor,R):-
	Minor == -1,
	getMinorPathDet(Ls,Elem,R), !.
getMinorPathDet([[LPath,Total]|Ls],[_,TotalMinor],R):-
	Total < TotalMinor,
	append([LPath],[Total],Minor),
	getMinorPathDet(Ls,Minor,R), !.
getMinorPathDet([_|Ls],Minor,R):-
	getMinorPathDet(Ls,Minor,R), !.

getMinorPath(L,Lf):-
	getMinorPathDet(L,-1,Lf),
	!.

%predicado getPathOptimal
%
% Retorna o melhor caminho de um POIStart para um POIEnd, considerando
% a rota com menor distancia e o menor numero de steps 
% entre pois
getPathOptimal(POIStart,POIEnd,Restriction,PathOptimal):-
	getAllListPathWTD(POIStart,POIEnd,Restriction,LAllPathWTD),
	getMinorPath(LAllPathWTD,PathOptimal).

%predicado combinatorial
%
% Calculo de um combinatorio entre membros da lista
% combinatorial(A,B).
%
%A: Lista de elementos
%B: Forma combinacoes
combinatorial(_,[]).
combinatorial([X|T],[X|Comb]):- combinatorial(T,Comb).
combinatorial([_|T],[X|Comb]):- combinatorial(T,[X|Comb]).

%predicado getAllCombinationPath
%
% Gera os melhores caminhos entre o primeiro POI da lista
% em relacao aos restantes elementos 
% getAllCombinationPath(A,B,C)
%
%A: Lista de POI
%B: Considera restricoes de acessibilidade?
%C: Lista resultado
getAllCombinationPathDet([X|Ls],Restriction,LTemp,Lf):-
	append([X],Ls,LPOI),
	combinatorial(LPOI,[X,Y]),
	getPathOptimal(X,Y,Restriction,Path),
	\+ member(Path,LTemp),
	getAllCombinationPathDet(LPOI,Restriction,[Path|LTemp],Lf), !.
getAllCombinationPathDet(_,_,LTemp,Lf):-
	reverse(LTemp,Lf),
	!.

getAllCombinationPath(LPOI,Restriction,L):-
	getAllCombinationPathDet(LPOI,Restriction,[],L),
	!.

%predicado getPOIEnd
%
% Retorna o POIEnd de uma estrada
% getPOIEnd(A,B).
%
%A: Caminho de POIStart para POIEnd
%B: POIEnd
getPOIEnd([Path,_],POIEnd):- last(Path,[_,POIEnd,_,_,_]), !.
getPOIEnd([[_,POIEnd,_,_,_],_],POIEnd):- !.

%predicado validateLPOI
%
% validacao do inicio da visita com os pois a visitar
% validateLPOI(A,B,C).
%
%A: Local de inicio
%B: Lista de pois a visitar
%C: Lista validada
validateLPOI(StartLocation,LPOI,LValidatedPOI):-
	\+ member(StartLocation,LPOI),
	append([StartLocation],LPOI,LValidatedPOI),
	!.
validateLPOI(StartLocation,LPOI,LValidatedPOI):-
	member(StartLocation,LPOI),
	delete(LPOI,StartLocation,LTempPOI),
	append([StartLocation],LTempPOI,LValidatedPOI),
	!.

%predicado getRoute
%
% Gera uma agenda para o turista
% genRouteDebug(A,B,C,D).
%
%A: ID POI Inicial
%B: Lista de pois do turista
%C: Considera restricoes de acessibilidade?
%D: Lista resultado
genRouteDet(StartLocation,[X],Restriction,LTemp,Lf):-
	getPathOptimal(X,StartLocation,Restriction,[FinalPath,FinalD]), !,
	append([X,StartLocation],[[FinalPath,FinalD]],FinalElem),
	reverse(LTemp,LTemp2),
	append(LTemp2,[FinalElem],Lf),
	!.
genRouteDet(StartLocation,[X|Ls],Restriction,LTemp,Lf):-
	append([X],Ls,LPOI),
	getAllCombinationPath(LPOI,Restriction,LAllPath),
	getMinorPath(LAllPath,LMinorPath),
	getPOIEnd(LMinorPath,Y),
	delete(Ls,Y,LPOIYRemoved),
	append([Y],LPOIYRemoved,NewLPOI),
	genRouteDet(StartLocation,NewLPOI,Restriction,[[X,Y,LMinorPath]|LTemp],Lf), !.

genRoute(StartLocation,LPOI,Restriction,L):-
	validateLPOI(StartLocation,LPOI,LValidatedPOI),
	genRouteDet(StartLocation,LValidatedPOI,Restriction,[],L),
	!.

%predicado calcTotalDuration
%
% Calculo da duracao da agenda do turista
% calcTotalDuration(A,B).
%
%A: Lista de pois
%B: Duracao total
calcTotalDuration([],0):- !.
calcTotalDuration([[POIStart,_,_]|Ls],Total):-
	calcTotalDuration(Ls,Subtotal),
	poi(POIStart,_,_,Duration),
	Total is Subtotal + Duration, !.

%predicado calcTotalDistance
%
% Calculo da distancia total da agenda do turista
% calcTotalDistance(A,B).
%
%A: Lista de pois
%B: Distancia total
calcTotalDistance([],0):- !.
calcTotalDistance([[_,_,[_,SubTotalDistance]]|Ls],Total):-
	calcTotalDistance(Ls,Subtotal),
	Total is Subtotal + SubTotalDistance, !.

%predicado printPath
%
% imprime rota desde POIStart ate POIEnd
% printPath(A).
%
%A: Lista a imprimir
printPath([]):- !.
printPath([[POIStart,POIEnd,Restriction,Distance,_]|Ls]):-
	write('De: '), write(POIStart), write(' Para: '), write(POIEnd), nl,
	write('Rua inclinada: '), write(Restriction), nl,
	write('Distancia: '), write(Distance), nl, nl,
	printPath(Ls).

%predicado printRoute
%
% Imprime agenda do turista
% printRoute(A).
%
%A: Lista de pois
printRoute([]):- !.
printRoute([[POIStart,POIEnd,[Path,TotalDistance]]|Ls]):-
	poi(POIStart,Location,LWH,Duration),
	write('Visitar POI '), write('('), write(POIStart), write(') '), nl,
	write(Location), write(' '), write('Horario: '), write(LWH), nl,
	write('Duração: '), write(Duration), nl, nl,
	poi(POIEnd,LocationEnd,_,_),
	write('Destino: '), write('POI ('), write(POIEnd), write(') '), nl,
	write(LocationEnd), nl,
	write('Distancia total: '), write(TotalDistance), nl, nl,
	write('Rota: '), nl,
	printPath(Path), nl,
	printRoute(Ls).

%predicado genRouteDebug
%
% Gera e imprime agenda do turista
% genRouteDebug(A,B,C).
%
%A: ID POI Inicial
%B: Lista de pois do turista
%C: Considera restricoes de acessibilidade?
genRouteDebug(StartLocation,LPOI,Restriction):-
	genRoute(StartLocation,LPOI,Restriction,L),
	printRoute(L),
	calcTotalDuration(L,TotalDuration),
	calcTotalDistance(L,TotalDistance),
	write('Duração total: '), write(TotalDuration), write(' min'), nl, nl,
	write('Distancia total: '), write(TotalDistance), write(' m'), nl,
	!.

%predicado genListIdPOILocationNames
%
% Obtem o ID de POI atraves do local
% genListIdPOILocationNames(A,B).
%
%A: Lista de localizações de POI
%B: Lista resultado
genListIdPOILocationNamesDet([],LTemp,L):-
	reverse(LTemp,L),
	!.
genListIdPOILocationNamesDet([X|Ls],LTemp,L):-
	getPOIByLocation(X,POIId),
	genListIdPOILocationNamesDet(Ls,[POIId|LTemp],L),
	!.

genListIdPOILocationNames(LPOILocationName,L):-
	genListIdPOILocationNamesDet(LPOILocationName,[],L),
	!.

%predicado genRouteDebugNameLocation
%
% Gera e imprime agenda do turista atraves do local
% genRouteDebugNameLocation(A,B,C).
%
%A: Nome local Inicial
%B: Lista de locais a visitar
%C: Considera restricoes de acessibilidade?
genRouteDebugNameLocation(StartLocationName,LPOINames,Restriction):-
	genListIdPOILocationNames(LPOINames,LPOI),
	getPOIByLocation(StartLocationName,StartLocation),
	genRouteDebug(StartLocation,LPOI,Restriction).

printPOI([]):- nl.
printPOI([poi(Id,Location,LWS,Duration)|Ls]):-
	write('ID: '), write(Id), nl,
	write('Local: '), write(Location), nl,
	write('Horario Funcionamento: '), write(LWS), nl,
	write('Duração: '), write(Duration), nl, nl,
	printPOI(Ls).

%showPOI predicate
%
% Imprime poi da base de conhecimento
showPOI:-
	getListPOI(L),
	write('POI disponível na base de conhecimento:'), nl,
	printPOI(L).


loadDataBase:-
	retractall(poi),
	retractall(road),
	consult('base.txt').

:- loadDataBase.
