﻿using System.Web;
using System.Web.Mvc;

namespace LAPR5_ALGAV
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
