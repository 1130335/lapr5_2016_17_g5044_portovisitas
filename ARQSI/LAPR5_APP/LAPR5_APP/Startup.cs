﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LAPR5_APP.Startup))]
namespace LAPR5_APP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
