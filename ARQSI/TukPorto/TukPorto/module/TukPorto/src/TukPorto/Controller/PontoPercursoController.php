<?php
namespace TukPorto\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use TukPorto\Services\WebApiServices;
use Zend\Json\Json;
use TukPorto\Model\PontoPercurso;

class PontoPercursoController extends AbstractActionController
{

    protected $pontopercursoTable;

    protected $percursoTable;

    public function indexAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        return new ViewModel(array(
            'pontospercurso' => $this->getPontoPercursoTable()->fetchAll($id),
            'id' => $id
        ));
    }

    public function poisAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
        
        // Check if turista is owner of percurso
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        
        $percurso = $this->getPercursoTable()->getPercurso($id);
        
        if ($_SESSION['turistaid'] == null || $_SESSION['turistaid'] != $percurso->turistaid) {
            return $this->redirect()->toRoute('turista');
        }
        
        $pois = WebApiServices::getPois();
        
        return new ViewModel(array(
            'pois' => $pois,
            'id' => $id
        ));
    }

    public function meteorologiasAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
        
        $ponto = $this->getPontoPercursoTable()->getPontoPercurso($id);
        
        $json = WebApiServices::getMeteoDatabyPoiId($ponto->idpoi);
        $meteorologias = Json::decode($json);
        
        return array(
            'ponto' => $ponto,
            'meteorologias' => $meteorologias
        );
    }

    public function addAction()
    {
        $idb = (string) $this->params()->fromRoute('id', 0);
        if (! $idb) {
            return $this->redirect()->toRoute('pois', array(
                'action' => 'index',
                'id' => $idb
            ));
        }
        
        $pieces = explode("_", $idb);
        $idpoi =  $pieces[0]; 
        $idpercurso  = $pieces[1]; 
        
        // Check if turista is owner of percurso
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $percurso = $this->getPercursoTable()->getPercurso($idpercurso);
        if ($_SESSION['turistaid'] == null || $_SESSION['turistaid'] != $percurso->turistaid) {
            return $this->redirect()->toRoute('turista');
        }
        
        $poi = WebApiServices::getPoi($idpoi);
        $ponto = new PontoPercurso();
        
        $ponto->idpoi = $idpoi;
        $ponto->idpercurso = $idpercurso;
        $ponto->nome = $poi['Name'];
        $ponto->descricao = $poi['Description'];
        $ponto->local = $poi['Local']['Name'];
        $ponto->gps_lat = $poi['Local']['GPS_Lat'];
        $ponto->gps_long = $poi['Local']['GPS_Long'];
        
        $this->getPontoPercursoTable()->savePontoPercurso($ponto);
        // Fazer redirect melhor
        return $this->redirect()->toRoute('pontopercurso', array('id' => $idpercurso));
        
    }

    public function poiAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
        
        $json = WebApiServices::getMeteoDatabyPoiId($id);
        $meteorologias = Json::decode($json);
        
        $jsonPoi = WebApiServices::getPoi($id);
        
        return array(
            'poi' => $jsonPoi,
            'meteorologias' => $meteorologias
        );
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('pontopercurso');
        }
        $request = $this->getRequest();
        
        $pontopercurso = $this->getPontoPercursoTable()->getPontoPercurso($id);
        // Check if turista is owner of percurso
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        
        $percurso = $this->getPercursoTable()->getPercurso($pontopercurso->idpercurso);
        
        if ($_SESSION['turistaid'] == null || $_SESSION['turistaid'] != $percurso->turistaid) {
            return $this->redirect()->toRoute('turista');
        }
        
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');
            if ($del == 'Sim') {
                $id = (int) $request->getPost('id');
                $this->getPontoPercursoTable()->deletePontoPercurso($id);
            }
            // Redirect to list of percursos
            return $this->redirect()->toRoute('pontopercurso', array(
                'id' => $percurso->id
            ));
        }
        return array(
            'id' => $id,
            'pontopercurso' => $this->getPontoPercursoTable()->getPontoPercurso($id)
        );
    }

    public function getPontoPercursoTable()
    {
        if (! $this->pontopercursoTable) {
            $sm = $this->getServiceLocator();
            $this->pontopercursoTable = $sm->get('TukPorto\Model\PontoPercursoTable');
        }
        
        return $this->pontopercursoTable;
    }

    public function getPercursoTable()
    {
        if (! $this->percursoTable) {
            $sm = $this->getServiceLocator();
            $this->percursoTable = $sm->get('TukPorto\Model\PercursoTable');
        }
        
        return $this->percursoTable;
    }
}

