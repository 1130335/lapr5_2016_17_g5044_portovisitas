<?php
namespace TukPorto\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use TukPorto\Form\LoginForm;
use TukPorto\Form\RegisterForm;
use TukPorto\Model\Turista;
use Zend\View\Model\ViewModel;

class TuristaController extends AbstractActionController
{

    protected $turistaTable;

    public function loginAction()
    {
        $request = $this->getRequest();
        if (! $request->isPost()) {
            $form = new LoginForm();
            $form->get('submit')->setValue('Login');
            return array(
                'form' => $form
            );
        } else {
            $name = $request->getPost('email');
            $pass = $request->getPost('password');
            
            $idTurista = $this->getTuristaTable()->getTuristaIdByEmail($name, $pass);
            
            if ($idTurista > 0) {
                if (session_status() == PHP_SESSION_NONE) {
                    session_start();
                }
                $_SESSION['turista'] = $name;
                $_SESSION['turistaid'] = $idTurista;
                return $this->redirect()->toRoute('home');
            }
            
            return $this->redirect()->toRoute('turista');
        }
    }

    public function logoutAction()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION['turista'] = null;
        $_SESSION['turistaid'] = null;
        return $this->redirect()->toRoute('home');
    }

    public function infoAction()
    {
        return new ViewModel(array(
            'turista' => $this->getTuristaTable()->getTuristaBySession()
        ));
    }

    public function registerAction()
    {
        $form = new RegisterForm();
        $form->get('submit')->setValue('Add');
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $turista = new Turista();
            $form->setInputFilter($turista->getInputFilter());
            $form->setData($request->getPost());
            
            if ($form->isValid()) {
                $turista->exchangeArray($form->getData());
                $this->getTuristaTable()->saveTurista($turista);
                
                // Redirect to list of percursos
                return $this->redirect()->toRoute('percurso');
            }
        }
        
        return array(
            'form' => $form
        );
    }

    public function getTuristaTable()
    {
        if (! $this->turistaTable) {
            $sm = $this->getServiceLocator();
            $this->turistaTable = $sm->get('TukPorto\Model\TuristaTable');
        }
        return $this->turistaTable;
    }
}

