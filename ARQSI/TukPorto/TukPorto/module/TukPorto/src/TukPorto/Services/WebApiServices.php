<?php
namespace TukPorto\Services;

use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Json\Json;

class WebApiServices
{

    public static $username = "tukporto@tuk.pt";

    public static $password = "TukPorto1!";

    public static $enderecoBase = 'https://localhost:44384';

    public static function Login()
    {
        $username = WebApiServices::$username;
        $password = WebApiServices::$password;
        $enderecoBase = WebApiServices::$enderecoBase;
        $client = new Client($enderecoBase . '/Token');
        $client->setMethod(Request::METHOD_POST);
        $data = "grant_type=password&username=$username&password=$password";
        $len = strlen($data);
        $client->setHeaders(array(
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Content-Length' => $len
        ));
        $client->setOptions([
            'sslverifypeer' => false
        ]);
        $client->setRawBody($data);
        $response = $client->send();
        $body = Json::decode($response->getBody());
        if (! empty($body->access_token)) {
            if (! isset($_SESSION)) {
                session_start();
            }
            $_SESSION['access_token'] = $body->access_token;
            $_SESSION['username'] = $username;
            return true;
        } else
            return false;
    }

    public static function Logout()
    {
        if (! isset($_SESSION)) {
            session_start();
        }
        $_SESSION['username'] = null;
        $_SESSION['access_token'] = null;
    }

    public static function getPois()
    {
        if (! isset($_SESSION)) {
            session_start();
        }
        if (! isset($_SESSION['access_token'])) {
            WebApiServices::Login();
        }
        
        $client = new Client(WebApiServices::$enderecoBase . '/api/POIs');
        $client->setMethod(Request::METHOD_GET);
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        $client->setHeaders(array(
            'Authorization' => $bearer_token
        ));
        $client->setOptions([
            'sslverifypeer' => false
        ]);
        $response = $client->send();
        $body = $response->getBody();
        $pois = Json::decode($response->getBody(), true);
        return $pois;
    }
    
    public static function getPoi($id){
        if (! isset($_SESSION)) {
            session_start();
        }
        if (! isset($_SESSION['access_token'])) {
            WebApiServices::Login();
        }
        
        $client = new Client(WebApiServices::$enderecoBase . '/api/POIs/' . $id);
        $client->setMethod(Request::METHOD_GET);
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        $client->setHeaders(array(
            'Authorization' => $bearer_token
        ));
        $client->setOptions([
            'sslverifypeer' => false
        ]);
        $response = $client->send();
        $body = $response->getBody();
        $poi = Json::decode($response->getBody(), true);
        return $poi;
    }

    public static function getMeteoDatabyPoiId($poiid)
    {
        if (! isset($_SESSION)) {
            session_start();
        }
        if (! isset($_SESSION['access_token'])) {
            WebApiServices::Login();
        }
        
        $date = str_replace('-', '/', date("Y-m-d"));
        $diaY = date('Y-m-d', strtotime($date . "-1 days"));
        $diaYY = date('Y-m-d', strtotime($date . "-2 days"));
        
        // $var = $_SESSION;
        $client = new Client(WebApiServices::$enderecoBase . '/api/Meteorologies/' . $poiid . '?&dataMin=' . $diaYY . '&dataMax=' . $diaY);
        $client->setMethod(Request::METHOD_GET);
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Authorization' => $bearer_token
        ));
        $client->setOptions([
            'sslverifypeer' => false
        ]);
        
        $response = $client->send();
        $body = $response->getBody();
        return $body;
    }
}

