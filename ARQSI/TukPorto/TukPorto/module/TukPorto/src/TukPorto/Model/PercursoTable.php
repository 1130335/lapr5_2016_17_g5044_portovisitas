<?php
namespace TukPorto\Model;

use Zend\Db\TableGateway\TableGateway;


class PercursoTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(array('*'));
        $sqlSelect->join('turista', 'turista.id = percurso.turistaid', array('email'), 'left');
        
        $statement = $this->tableGateway->getSql()->prepareStatementForSqlObject($sqlSelect);
        $resultSet = $statement->execute();
        return $resultSet;
        
    }

    public function getPercurso($id)
    {
        $id = (int) $id;
        $rowSet = $this->tableGateway->select(array(
            'id' => $id
        ));
        $row=$rowSet->current();
        if (! $row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
    
   
    public function savePercurso(Percurso $percurso)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        
        $data = array(
            'descricao' => $percurso->descricao,
            'data' => $percurso->data,
            'turistaid' => $_SESSION['turistaid']
        );
        
        $id = (int) $percurso->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getPercurso($id)) {
                $this->tableGateway->update($data, array(
                    'id' => $id
                ));
                
            } else {
                throw new \Exception("Percurso id does not exist");
            }
        }
    }

    public function deletePercurso($id)
    {
        $this->tableGateway->delete(array(
            'id' => (int) $id
        ));
    }
}

