<?php
namespace TukPorto\Model;

use Zend\Db\TableGateway\TableGateway;

class PontoPercursoTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($id)
    {
        $resultSet = $this->tableGateway->select(array(
            'idpercurso' => $id
        ));
        return $resultSet;
    }

    public function getPontoPercurso($id)
    {
        $id = (int) $id;
        $rowSet = $this->tableGateway->select(array(
            'id' => $id
        ));
        $row=$rowSet->current();
        if (! $row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function savePontoPercurso(PontoPercurso $pontopercurso)
    {
        $data = array(
            'id' => $pontopercurso->id,
            'idpoi' => $pontopercurso->idpoi,
            'idpercurso' => $pontopercurso->idpercurso,
            'nome' => $pontopercurso->nome,
            'local' => $pontopercurso->local,
            'descricao' => $pontopercurso->descricao,
            'gps_lat' => $pontopercurso->gps_lat,
            'gps_long' => $pontopercurso->gps_long
        );
        
        $id = (int) $pontopercurso->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getPontoPercurso($id)) {
                $this->tableGateway->update($data, array(
                    'id' => $id
                ));
            } else {
                throw new \Exception("Ponto Percurso id does not exist");
            }
        }
    }

    public function deletePontoPercurso($id)
    {
        $this->tableGateway->delete(array(
            'id' => (int) $id
        ));
    }
}

