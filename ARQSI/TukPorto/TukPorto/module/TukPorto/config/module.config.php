<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'TukPorto\Controller\Percurso' => 'TukPorto\Controller\PercursoController',
            'TukPorto\Controller\Turista' => 'TukPorto\Controller\TuristaController',
            'TukPorto\Controller\PontoPercurso' => 'TukPorto\Controller\PontoPercursoController'
        )
    ),
    'router' => array(
        'routes' => array(
            'turista' => array(
                'type' => 'segment',
                'options' => array(
                    // Change this to something specific to your module
                    'route' => '/turista[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'TukPorto\Controller\Turista',
                        'action' => 'login'
                    )
                )
            ),
            'pontopercurso' => array(
                'type' => 'segment',
                'options' => array(
                    // Change this to something specific to your module
                    'route' => '/pontoPercurso[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9_]+'
                    ),
                    'defaults' => array(
                        'controller' => 'TukPorto\Controller\PontoPercurso',
                        'action' => 'index'
                    )
                )
            ),
            'percurso' => array(
                'type' => 'segment',
                'options' => array(
                    // Change this to something specific to your module
                    'route' => '/percurso[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'TukPorto\Controller\Percurso',
                        'action' => 'index'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    // This route is a sane default when developing a module;
                    // as you solidify the routes for your module, however,
                    // you may want to remove it and replace it with more
                    // specific routes.
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                            ),
                            'defaults' => array()
                        )
                    )
                )
            )
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'TukPorto' => __DIR__ . '/../view'
        )
    )
);
