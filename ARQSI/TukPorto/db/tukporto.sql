-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 11, 2016 at 10:27 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tukporto`
--

-- --------------------------------------------------------

--
-- Table structure for table `percurso`
--

CREATE TABLE `percurso` (
  `id` int(10) NOT NULL,
  `descricao` varchar(1000) NOT NULL,
  `data` date NOT NULL,
  `turistaid` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `percurso`
--

INSERT INTO `percurso` (`id`, `descricao`, `data`, `turistaid`) VALUES
(23, 'woof', '0444-02-03', 8),
(24, 'woof', '0005-05-05', 8),
(26, 'sfasfa', '0004-02-03', 8),
(27, 'dsadad', '0004-02-03', 8),
(28, 'asdfsa', '1337-06-09', 8);

-- --------------------------------------------------------

--
-- Table structure for table `pontopercurso`
--

CREATE TABLE `pontopercurso` (
  `id` int(10) NOT NULL,
  `idpoi` int(10) NOT NULL,
  `nome` varchar(300) NOT NULL,
  `descricao` varchar(1000) NOT NULL,
  `gps_lat` float NOT NULL,
  `gps_long` float NOT NULL,
  `idpercurso` int(10) NOT NULL,
  `local` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pontopercurso`
--

INSERT INTO `pontopercurso` (`id`, `idpoi`, `nome`, `descricao`, `gps_lat`, `gps_long`, `idpercurso`, `local`) VALUES
(2, 13, 'xxx', 'xx', 11, 1, 22, 'Local 1'),
(3, 13, 'sdasdasd', 'asdasdasd', 22, 22, 22, 'Local 2'),
(5, 7, 'sample string 3', 'sample string 4', -90, -120, 22, 'Local 1'),
(6, 3, 'Name 3', 'Des3', 0, 180, 22, 'Local 2'),
(7, 3, 'Name 3', 'Des3', 0, 180, 22, 'Local 2'),
(8, 3, 'Name 3', 'Des3', 0, 180, 22, 'Local 2'),
(10, 5, 'sample string 3', 'sample string 4', -90, -120, 22, 'Local 1'),
(11, 3, 'Name 3', 'Des3', 0, 180, 28, 'Local 2');

-- --------------------------------------------------------

--
-- Table structure for table `turista`
--

CREATE TABLE `turista` (
  `id` int(10) NOT NULL,
  `nome` varchar(300) NOT NULL,
  `nacionalidade` varchar(100) NOT NULL,
  `email` varchar(320) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `turista`
--

INSERT INTO `turista` (`id`, `nome`, `nacionalidade`, `email`, `password`) VALUES
(2, 'meow', 'meow', 'meow@meow.pt', 'meow'),
(8, 'Test', 'PT', 'teste@test.pt', 'dd');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `percurso`
--
ALTER TABLE `percurso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `turistaid` (`turistaid`);

--
-- Indexes for table `pontopercurso`
--
ALTER TABLE `pontopercurso`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `turista`
--
ALTER TABLE `turista`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `percurso`
--
ALTER TABLE `percurso`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `pontopercurso`
--
ALTER TABLE `pontopercurso`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `turista`
--
ALTER TABLE `turista`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `percurso`
--
ALTER TABLE `percurso`
  ADD CONSTRAINT `percurso_ibfk_1` FOREIGN KEY (`turistaid`) REFERENCES `turista` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
