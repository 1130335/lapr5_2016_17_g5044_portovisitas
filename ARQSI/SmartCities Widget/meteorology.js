//globle variables
var meteoResponse;

function meteorologiaButton() {
    var rButton = document.createElement("input");

    var atributes = [
        ["type", "radio"],
        ["name", "sensor"],
        ["id", "meteorologia"],
        ["onchange", "requestwithAuth(\"GET\", \"https://localhost:44384/api/Meteorologies\", \"\", \"JSON\", getMinMaxArray, null)"]
    ];
    setAttributes(rButton, atributes);

    var rButtonName = document.createElement("widget");
    rButtonName.setAttribute("class", "tooltip");
    rButtonName.appendChild(document.createTextNode("Meteorologia"));
    var rButtonToolTip = document.createElement("span");
    rButtonToolTip.setAttribute("class", "tooltiptext");
    rButtonToolTip.appendChild(document.createTextNode("Sensor de Meteorologia"));
    rButtonName.appendChild(rButtonToolTip);

    sensores.appendChild(rButton);
    sensores.appendChild(rButtonName);
    sensores.appendChild(document.createElement("br"));
}

var token;

var min =
        {TimeOfReading: "",
            HourOfReading: "",
            Temperature: "",
            Wind: "",
            Humidity: "",
            Pressure: "",
            NO: "",
            NO2: "",
            CO2: ""
        };
var max =
        {TimeOfReading: "", HourOfReading: "",
            Temperature: "",
            Wind: "",
            Humidity: "",
            Pressure: "",
            NO: "",
            NO2: "",
            CO2: ""
        };

function requestwithAuth(type, url, params, dataType, func, args) {
    //Authentication
    if (token === undefined) {
        requestAuthToken(func, args);
        return;
    }

    var xmlHttpObj = new XMLHttpRequest();
    if (xmlHttpObj) {
        xmlHttpObj.onreadystatechange = function () {
            //Token Expired
            if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 401) {
                token = undefined;
                requestAuthToken();
            }
            //Valid  token
            if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200) {
                var response;
                switch (dataType) {
                    case "XML":
                        response = xmlHttpObj.responseXML;
                        break;
                    case "JSON":
                        response = JSON.parse(xmlHttpObj.responseText);
                        break;
                    default:
                        return;
                }
                func(response, args);
            }
        };
        xmlHttpObj.open(type, url + params, true);
        xmlHttpObj.setRequestHeader("Authorization", "Bearer " + token);
        xmlHttpObj.send(null);
    }
}

function requestAuthToken() {
    var xmlHttpObj = new XMLHttpRequest();

    xmlHttpObj.onreadystatechange = function () {
        if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200) {
            var response;
            response = JSON.parse(xmlHttpObj.responseText);
            token = response.access_token;
            requestwithAuth("GET", "https://localhost:44384/api/Meteorologies", "", "JSON", getMinMaxArray, null);
        }
    };

    xmlHttpObj.open("POST", "https://localhost:44384/Token", true);
    xmlHttpObj.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttpObj.send("grant_type=password&username=widget@widget.com&password=widgetPassword2016!");
}


function getMinMaxArray(request, args) {
    meteoResponse = request;
    if (meteoResponse.length > 0) {
        var first = meteoResponse[0];
        var length = Object.keys(first).length;

        //init with first
        for (var j = 2; j < length - 1; j++) {
            var attr = Object.keys(first)[j];
            var value = first[attr];
            if (attr == "HourOfReading") {
                value = value.split("T")[1];
                first[attr] = value;
            }
            if (attr == "TimeOfReading") {
                value = value.split("T")[0];
                first[attr] = value;
            }

            min[attr] = value;
            max[attr] = value;
        }

        //init with rest
        for (var i = 1; i < meteoResponse.length; i++) {
            var meteo = meteoResponse[i];
            for (var j = 2; j < length - 1; j++) {
                var attr = Object.keys(meteo)[j];
                var value = meteo[attr];

                //Fix Time and Hour
                if (attr == "HourOfReading") {
                    value = value.split("T")[1];
                    meteo[attr] = value;
                }
                if (attr == "TimeOfReading") {
                    value = value.split("T")[0];
                    meteo[attr] = value;
                }

                if (min[attr] > value) {
                    min[attr] = value;
                }
                if (max[attr] < value) {
                    max[attr] = value;
                }
            }
        }
        DrawMeteorologiaFilter();
    }

}

function DrawMeteorologiaFilter() {
    clearFacetas();
    var first = meteoResponse[0];
    for (var i = 0; i < Object.keys(first).length; i++) {
        var container = document.createElement("widget");
        var element = document.createElement("widget");
        element.setAttribute("style", "display:none");

        var attr = Object.keys(first)[i];
        if (attr == "MeteorologyID" || attr == "LocalID") {
            continue;
        }
        var checkbox = document.createElement("input");

        var atributes = [
            ["type", "checkbox"],
            ["name", attr],
            ["onchange", "triggerFaceta(this)"]
        ];
        setAttributes(checkbox, atributes);

        container.appendChild(checkbox);
        container.appendChild(document.createTextNode(attr));
        if (attr == "Local") {
            addMeteorologiaLocais(element);
        } else {

            var form1, form2;
            switch (attr) {
                case "TimeOfReading":
                    var form1 = createTimeForm("    De:");
                    var form2 = createTimeForm("   Ate:");
                    element.appendChild(form1);
                    element.appendChild(form2);
                    break;
                case "HourOfReading":
                    var form1 = createHourForm("    De:");
                    var form2 = createHourForm("   Ate:");
                    element.appendChild(form1);
                    element.appendChild(form2);
                    break;
                default:
                    var form1 = createSliderForm("    De:");
                    var form2 = createSliderForm("   Ate:");
                    element.appendChild(form1);
                    element.appendChild(form2);

                    form1.childNodes[2].nodeValue = min[attr];

                    form2.childNodes[2].nodeValue = max[attr];
                    break;
            }
            form1.childNodes[1].min = min[attr];
            form1.childNodes[1].max = max[attr];
            form2.childNodes[1].min = min[attr];
            form2.childNodes[1].max = max[attr];
            form1.childNodes[1].value = min[attr];
            form2.childNodes[1].value = max[attr];
        }
        container.appendChild(element);
        container.appendChild(document.createElement("br"));

        facetas.appendChild(container);
    }

    addButtonsF("meteorologiaValidate()");
}

function addMeteorologiaLocais(element) {
    for (var i = 0; i < meteoResponse.length; i++) {
        var checkbox = document.createElement("input");
        checkbox.setAttribute("type", "checkbox");
        checkbox.setAttribute("name", meteoResponse[i].Local.Name);
        element.appendChild(document.createElement("br"));
        element.appendChild(document.createTextNode("   "));
        element.appendChild(checkbox);
        element.appendChild(document.createTextNode(meteoResponse[i].Local.Name));
    }
}

function meteorologiaValidate() {
    clearResults();
    var newResponse = meteoResponse.slice();

    var facetas = document.getElementById("facetas");
    for (var i = 0; i < facetas.childElementCount - 2; i++) {
        var child = facetas.childNodes[i];
        if (child.firstChild.checked) { //Check if checkbox is selected
            var container = child.childNodes[2].getElementsByTagName("input");
            switch (container[0].type) {
                case "checkbox":
                    var checkArray = [newResponse.length];
                    for (var j = 0; j < newResponse.length; j++) {
                        checkArray[j] = false;
                    }
                    for (var j = 0; j < container.length; j++) {
                        if (container[j].checked) {
                            for (var t = 0; t < newResponse.length; t++) {
                                if (newResponse[t][child.firstChild.name]["Name"] == container[j].name) {
                                    checkArray[t] = true;
                                }
                            }
                        }
                    }
                    for (var t = 0; t < checkArray.length; t++) {
                        if (!checkArray[t]) {
                            newResponse.splice(t, 1);
                            checkArray.splice(t, 1);
                            t--;
                        }
                    }
                    break;
                case "time":
                case "date":
                    var min = container[0].value;
                    var max = container[1].value;
                    if (container[0].type == "time") {
                        if (min.split(":").length < 3) {
                            min += ":00";
                        }
                        if (max.split(":").length < 3) {
                            max += ":00";
                        }
                    }
                    for (var j = 0; j < newResponse.length; j++) {
                        var value = newResponse[j];
                        var valueN = value[child.firstChild.name];
                        if (valueN < min || valueN > max) {
                            newResponse.splice(j, 1);
                            j--;
                        }
                    }
                    break;
                default:
                    var min = parseFloat(container[0].value);
                    var max = parseFloat(container[1].value);
                    for (var j = 0; j < newResponse.length; j++) {
                        var value = newResponse[j];
                        var valueF = parseFloat(value[child.firstChild.name]);
                        if (valueF < min || valueF > max) {
                            newResponse.splice(j, 1);
                            j--;
                        }
                    }
                    break;
            }
        }
    }
    drawResultsMeteorologia(newResponse);
}

function drawResultsMeteorologia(results) {
    if (results.length == 0) {
        return;
    }

    var maindiv = document.getElementById("widget_vertical");
    var right_div = document.createElement("div");
    right_div.setAttribute("id", "results");
    right_div.setAttribute("class", "results");
    maindiv.appendChild(right_div);
    var table = document.createElement("table");
    table.setAttribute("border", "dotted");

    //titles
    var titleTR = document.createElement("tr");
    titleTR.setAttribute("align", "center");
    var first = results[0];
    var length = Object.keys(first).length;

    //Manually add Local
    var th = document.createElement("th");
    th.appendChild(document.createTextNode(Object.keys(first)[0]));
    titleTR.appendChild(th);
    for (var i = 2; i < length - 1; i++) { //IGNORAR ID?????
        var th = document.createElement("th");
        th.appendChild(document.createTextNode(Object.keys(first)[i]));
        titleTR.appendChild(th);
    }
    table.appendChild(titleTR);

    //data
    for (var i = 0; i < results.length; i++) {
        var meteo = results[i];
        var anotherTR = document.createElement("tr");
        anotherTR.setAttribute("align", "center");

        //Manually add Local
        var td = document.createElement("td");
        td.appendChild(document.createTextNode(meteo[Object.keys(meteo)[0]].Name));
        anotherTR.appendChild(td);
        for (var j = 2; j < length - 1; j++) {
            var td = document.createElement("td");
            td.appendChild(document.createTextNode(meteo[Object.keys(meteo)[j]]));
            anotherTR.appendChild(td);
        }
        table.appendChild(anotherTR);
    }

    right_div.appendChild(table);
}