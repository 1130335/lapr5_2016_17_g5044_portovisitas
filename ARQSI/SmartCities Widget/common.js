function request(type, url, params, dataType, func, args) {
    var xmlHttpObj = new XMLHttpRequest();
    if (xmlHttpObj) {
        xmlHttpObj.onreadystatechange = function () {
            if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200) {
                var response;
                switch (dataType) {
                    case "XML":
                        response = xmlHttpObj.responseXML;
                        break;
                    case "JSON":
                        response = JSON.parse(xmlHttpObj.responseText);
                        break;
                    default:
                        return;
                }
                func(response, args);
            }
        };
        xmlHttpObj.open(type, url + params, true);
        xmlHttpObj.send(null);
    }
}

function clearFacetas() {
    var element = document.getElementById("facetas");
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}
function clearResults() {
    var results = document.getElementById("results");
    if (results != null) {
        results.parentNode.removeChild(results);
    }
}

function setAttributes(elem, attr) {
    for (var i = 0; i < attr.length; i++) {
        elem.setAttribute(attr[i][0], attr[i][1]);
    }
}

function getSensors() {
    widget = document.getElementById("widget_vertical");
    var div = document.createElement("div");

    div.setAttribute("id", "filter");
    div.setAttribute("class", "filters");
    widget.appendChild(div);
    widget = div;
    sensores = document.createElement("form");
    sensores.setAttribute("id", "sensores");
    facetas = document.createElement("form");
    facetas.setAttribute("id", "facetas");

    widget.appendChild(document.createTextNode("Sensores:"));
    widget.appendChild(document.createElement("br"));
    widget.appendChild(sensores);
    widget.appendChild(document.createElement("br"));
    widget.appendChild(document.createTextNode("Facetas:"));
    widget.appendChild(facetas);

    meteorologiaButton();
    request("GET", "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/sensores.php", "", "XML", sensoresResponseHandler, null);
}

function triggerFaceta(checkbox) {
    if (checkbox.checked) {
        checkbox.nextSibling.nextSibling.style = "display:inline";
    } else {
        checkbox.nextSibling.nextSibling.style = "display:none";
    }
}

function createTimeForm(text) {
    var timeForm = document.createElement("form");
    var inputTimeForm = document.createElement("input");

    var atributes = [
        ["type", "date"],
        ["min", "0"],
        ["max", "0"]
    ];
    setAttributes(inputTimeForm, atributes);

    timeForm.appendChild(document.createTextNode(text));
    timeForm.appendChild(inputTimeForm);
    return timeForm;
}

function createHourForm(text) {
    var hourForm = document.createElement("form");
    var inputHourForm = document.createElement("input");
    inputHourForm.setAttribute("type", "time");
    inputHourForm.setAttribute("step", "1");
    hourForm.appendChild(document.createTextNode(text));
    hourForm.appendChild(inputHourForm);
    return hourForm;
}

function createSimpleTextNumberForm(text) {
    var numberForm = document.createElement("form");
    var inputnumberForm = document.createElement("input");
    inputnumberForm.setAttribute("type", "text");
    numberForm.appendChild(document.createTextNode(text));
    numberForm.appendChild(inputnumberForm);
    return numberForm;
}

function createSimpleNumberForm(text, step) {
    var numberForm = document.createElement("form");
    var inputnumberForm = document.createElement("input");

    var atributes = [
        ["type", "number"],
        ["min", "0"],
        ["max", "0"],
        ["step", step]
    ];
    setAttributes(inputnumberForm, atributes);

    numberForm.appendChild(document.createTextNode(text));
    numberForm.appendChild(inputnumberForm);
    return numberForm;
}

function createSliderForm(text) {
    var sliderForm = document.createElement("form");
    var inputSliderForm = document.createElement("input");
    var textNode = document.createTextNode("");

    var atributes = [
        ["type", "range"],
        ["min", "0"],
        ["max", "0"],
        ["step", 0.000001],
        ["value", 0],
        ["onchange", "showValue(this)"]
    ];
    setAttributes(inputSliderForm, atributes);

    sliderForm.appendChild(document.createTextNode(text));
    sliderForm.appendChild(inputSliderForm);
    sliderForm.appendChild(textNode);
    sliderForm.appendChild(document.createElement("br"));

    showValue(inputSliderForm);
    return sliderForm;
}

function addButtonsF(functionname) {
    var button = document.createElement("input");

    var atributes = [
        ["type", "button"],
        ["onclick", functionname],
        ["value", "submit"]
    ];
    setAttributes(button, atributes);
    facetas.appendChild(button);
}