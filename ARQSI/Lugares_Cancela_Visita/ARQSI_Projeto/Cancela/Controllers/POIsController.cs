﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;
using Microsoft.AspNet.Identity;
using System.Diagnostics;

namespace NewNewCancela.Controllers
{
    [AllowAnonymous]
    public class POIsController : ApiController
    {
        private DatumDbContext db = new DatumDbContext();

        // GET: api/POIs
        public IEnumerable<PoiDtoSend> GetPOIs()
        {
            List<PoiDtoSend> newList = new List<PoiDtoSend>();
            IEnumerable<POI> list = db.POIs.ToList();
            foreach (var poi in list)
            {
                newList.Add(new PoiDtoSend(poi));
            }
            return newList;
        }

        // GET: api/POIs/5
        [ResponseType(typeof(PoiDtoSend))]
        [Authorize(Roles = "Administrador")]
        public IHttpActionResult GetPOI(int id)
        {
            POI pOI = db.POIs.Find(id);
            if (pOI == null)
            {
                return NotFound();
            }

            return Ok(new PoiDtoSend(pOI));
        }

        // PUT: api/POIs/5
        [ResponseType(typeof(void))]
        [Authorize(Roles = "Administrador")]
        public IHttpActionResult PutPOI(int id, POIDtoReceive pOIDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pOIDTO.POIID)
            {
                return BadRequest();
            }

            POI poi = db.POIs.Find(id);

            if (poi.CheckNotOwner(User.Identity.GetUserId()))
            {
                return StatusCode(HttpStatusCode.Unauthorized);
            }

            copyToDto(poi, pOIDTO);
            db.Entry(poi).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!POIExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/POIs
        [ResponseType(typeof(PoiDtoSend))]
        [Authorize(Roles = "Administrador")]
        public IHttpActionResult PostPOI(POIDtoReceive poiDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            POI poi = new POI();
            copyToDto(poi, poiDto);

            db.POIs.Add(poi);
            db.SaveChanges();
            poiDto.POIID = poi.POIID;

            return CreatedAtRoute("DefaultApi", new { id = poiDto.POIID }, poiDto);
        }

        // DELETE: api/POIs/5
        [ResponseType(typeof(void))]
        [Authorize(Roles = "Administrador")]
        public IHttpActionResult DeletePOI(int id)
        {
            POI pOI = db.POIs.Find(id);
            if (pOI == null)
            {
                return NotFound();
            }

            if (pOI.CheckNotOwner(User.Identity.GetUserId()))
            {
                return StatusCode(HttpStatusCode.Unauthorized);
            }


            db.POIs.Remove(pOI);
            db.SaveChanges();

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void copyToDto(POI poi, POIDtoReceive poiDto)
        {
            if (poiDto.Name != null)
            {
                poi.Name = poiDto.Name;
            }
            if (poiDto.Description != null)
            {
                poi.Description = poiDto.Description;
            }
            Local local = db.Locals.Find(poiDto.LocalID);
            if (local != null)
            {
                poi.LocalID = poiDto.LocalID;
                poi.Local = local;
            }
            poi.UserId = User.Identity.GetUserId();
            poi.User = db.Users.Find(poi.UserId);
        }

        private bool POIExists(int id)
        {
            return db.POIs.Count(e => e.POIID == id) > 0;
        }
    }
}