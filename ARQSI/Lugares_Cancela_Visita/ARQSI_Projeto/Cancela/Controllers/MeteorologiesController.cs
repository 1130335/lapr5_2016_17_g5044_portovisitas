﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;
using System.Diagnostics;

namespace NewNewCancela.Controllers
{

    [Authorize(Roles = "Editor")]
    public class MeteorologiesController : ApiController
    {
        private DatumDbContext db = new DatumDbContext();

        // GET: api/Meteorologies
        public IQueryable<Meteorology> GetMeteorologias()
        {
            return db.Meteorologias;
        }

        // GET: api/Meteorologies/5
        [ResponseType(typeof(Meteorology))]
        public IHttpActionResult GetMeteorology(int id)
        {
            Meteorology meteorology = db.Meteorologias.Find(id);
            if (meteorology == null)
            {
                return NotFound();
            }

            return Ok(meteorology);
        }

        // GET: api/Meteorologies/5?dataMax=xxxx&dataMin=xxx
        public IEnumerable<Meteorology> GetMeteorology(int id, string dataMax, string dataMin)
        {
            POI poi = db.POIs.Find(id);

            DateTime dtMin = Convert.ToDateTime(dataMin);
            DateTime dtMax = Convert.ToDateTime(dataMax);

            var meteorologies = from m in db.Meteorologias
                                where m.LocalID == poi.LocalID
                                select m;

            List<Meteorology> meteorologiesFilteredTime = new List<Meteorology>();

            Meteorology min = new Meteorology(), max = new Meteorology();

            min.Temperature = float.MaxValue; max.Temperature = float.MinValue;
            min.Wind = float.MaxValue; max.Wind = float.MinValue;
            min.Humidity = float.MaxValue; max.Humidity = float.MinValue;

            bool check = true;
            foreach (var meteo in meteorologies)
            {
                if (meteo.TimeOfReading.Date <= dtMax.Date 
                    && meteo.TimeOfReading.Date >= dtMin.Date )
                {
                    check = false;
                   
                    min.Temperature = (meteo.Temperature < min.Temperature ? meteo.Temperature : min.Temperature);
                    max.Temperature = (meteo.Temperature > max.Temperature ? meteo.Temperature : max.Temperature);
                    min.Wind = (meteo.Wind < min.Wind ? meteo.Wind : min.Wind);
                    max.Wind = (meteo.Wind > max.Wind ? meteo.Wind : max.Wind);
                    min.Humidity = (meteo.Humidity < min.Humidity ? meteo.Humidity : min.Humidity);
                    max.Humidity = (meteo.Humidity > max.Humidity ? meteo.Humidity : max.Humidity);
                }
            }

            if (check)
            {
                min = null;
                max = null;
            }

            meteorologiesFilteredTime.Add(min);
            meteorologiesFilteredTime.Add(max);


            return meteorologiesFilteredTime;
        }


        // PUT: api/Meteorologies/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMeteorology(int id, Meteorology meteorology)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != meteorology.MeteorologyID)
            {
                return BadRequest();
            }

            db.Entry(meteorology).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MeteorologyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Meteorologies
        [ResponseType(typeof(Meteorology))]
        public IHttpActionResult PostMeteorology(Meteorology meteorology)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Meteorologias.Add(meteorology);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = meteorology.MeteorologyID }, meteorology);
        }

        // DELETE: api/Meteorologies/5
        [ResponseType(typeof(Meteorology))]
        public IHttpActionResult DeleteMeteorology(int id)
        {
            Meteorology meteorology = db.Meteorologias.Find(id);
            if (meteorology == null)
            {
                return NotFound();
            }

            db.Meteorologias.Remove(meteorology);
            db.SaveChanges();

            return Ok(meteorology);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MeteorologyExists(int id)
        {
            return db.Meteorologias.Count(e => e.MeteorologyID == id) > 0;
        }



    }
}