﻿using DataAccessLibrary.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using NewVisita.Helpers;

namespace NewVisita.Models
{
    public class DataHelper : IValidatableObject
    {
        [Required]
        public DateTime TimeOfReadingMin { get; set; }
        [Required]
        public DateTime HourOfReadingMin { get; set; }
        [Required]
        public DateTime TimeOfReadingMax { get; set; }
        [Required]
        public DateTime HourOfReadingMax { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }        public int LocalID { get; set; }
        public string Local { get; set; }
        public float GPS_Lat { get; set; }
        public float GPS_Long { get; set; }
        public IEnumerable<Meteorology> listMeteo { get; set; }

        public IEnumerable<Meteorology> ValidMeteorologies()
        {
            IEnumerable<Meteorology> meteos = getMeteorologiesFromAPI(LocalID);
            IEnumerable<Meteorology> filteredList = checkAndRemoveInvalidDates(meteos);


            //If first attempt is successful
            if (filteredList.Any())
            {
                return filteredList;
            }
            else
            {
                return closestToLocal();
            }
        }

        public IEnumerable<Meteorology> getMeteorologiesFromAPI(int IDToCheck)
        {
            HttpClient requester = WebApiHttpClient.GetClient();
            var response = requester.GetAsync("api/Meteorologies?LocalID=" + IDToCheck).Result;
            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                var meteos =
                JsonConvert.DeserializeObject<IEnumerable<Meteorology>>(content);

                return meteos;
            }
            return null;
        }

        public IEnumerable<Meteorology> closestToLocal()
        {
            HttpClient requester = WebApiHttpClient.GetClient();
            var response = requester.GetAsync("api/Locals").Result;
            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                var locals =
                JsonConvert.DeserializeObject<List<Local>>(content);

                locals.Sort((x, y) => getDistanceToLocal(x).CompareTo(getDistanceToLocal(y)));

                foreach (var item in locals)
                {
                    IEnumerable<Meteorology> meteo = checkAndRemoveInvalidDates(getMeteorologiesFromAPI(item.LocalID));
                    if (meteo.Any())
                    {
                        return meteo;
                    }
                }
            }
            return null;
        }

        public List<Local> sortLocalsByDistance(List<Local> listLocals)
        {
            List<Local> newList = new List<Local>();
            foreach (var item in listLocals)
            {
                float distanceToPoint = getDistanceToLocal(item);
                int i;
                for (i = 0; i < newList.Count(); i++)
                {
                    Local local = newList.ElementAt(i);
                    if (distanceToPoint < getDistanceToLocal(local))
                    {
                        newList.Insert(i, item);
                        i = -1;
                        break;
                    }
                }
                if (i == -1)
                {
                    newList.Insert(i, item);
                }
            }
            return newList;
        }

        public float getDistanceToLocal(Local local)
        {
            float NewLong = local.GPS_Long - GPS_Long;
            float NewLat = local.GPS_Lat - GPS_Lat;
            return (float)Math.Sqrt(Math.Pow(NewLong, 2) + Math.Pow(NewLat, 2));
        }

        public IEnumerable<Meteorology> checkAndRemoveInvalidDates(IEnumerable<Meteorology> meteos)
        {
            List<Meteorology> newList = new List<Meteorology>();
            foreach (var item in meteos)
            {

                if (
                    item.TimeOfReading.Date.CompareTo(TimeOfReadingMin.Date) >= 0 &&
                    item.TimeOfReading.Date.CompareTo(TimeOfReadingMax.Date) <= 0 &&
                    TimeSpan.Compare(item.HourOfReading.TimeOfDay, HourOfReadingMin.TimeOfDay) >= 0 &&
                    TimeSpan.Compare(item.HourOfReading.TimeOfDay, HourOfReadingMax.TimeOfDay) <= 0
                    )
                {
                    newList.Add(item);
                }
            }
            return newList.AsEnumerable();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (TimeOfReadingMin > TimeOfReadingMax)
            {
                yield return
                  new ValidationResult(errorMessage: "Maximum date must be greater than minimum date",
                                       memberNames: new[] { "TimeOfReadingMax" });
            }
            if (HourOfReadingMin.TimeOfDay > HourOfReadingMax.TimeOfDay)
            {
                yield return
                  new ValidationResult(errorMessage: "Maximum time must be greater than minimum time",
                                       memberNames: new[] { "HourOfReadingMax" });
            }
        }
    }



}