﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataAccessLibrary.Models;
using NewVisita.Models;
using NewVisita.Helpers;
using Newtonsoft.Json;
using System.Net.Http;

namespace Visita.Controllers
{
    [LoggedIn]
    public class QuerysController : Controller
    {
        public ActionResult POI([Bind(Include = "POIID,Name,Description,LocalID,Local")] DataHelper data)
        {
            if (data == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return View(data);
        }

        [HttpPost]
        public ActionResult GetResults([Bind(Include = "Name,Description,Local,LocalID, TimeOfReadingMin, TimeOfReadingMax, HourOfReadingMin, HourOfReadingMax, Description")] DataHelper data)
        {
            if (ModelState.IsValid)
            {
                IEnumerable<Meteorology> meteos = data.ValidMeteorologies();
                if (meteos == null)
                {
                    return View("~/Views/Querys/NotFound.cshtml");
                }
                return View(meteos);
            }
            return View("POI", data);
        }

    }
}
