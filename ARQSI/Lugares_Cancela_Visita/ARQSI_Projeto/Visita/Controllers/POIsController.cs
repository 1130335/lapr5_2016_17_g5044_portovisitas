﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataAccessLibrary.Models;
using NewVisita.Models;
using NewVisita.Helpers;
using Newtonsoft.Json;
using System.Net.Http;

namespace Visita.Controllers
{
    [LoggedIn]
    public class POIsController : Controller
    {

        string url = "api/POIs/";

        // GET: Meteorologies
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var pois =
                JsonConvert.DeserializeObject<IEnumerable<POI>>(content);

                return View(pois);
            }
            else
            {
                return Content("An error has occurred: " + response.StatusCode);
            }

        }


    }
}
