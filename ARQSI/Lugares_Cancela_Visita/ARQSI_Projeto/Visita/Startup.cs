﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NewVisita.Startup))]
namespace NewVisita
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
