﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;
using Microsoft.AspNet.Identity;

namespace NewNewLugares.Controllers
{
    [Authorize(Roles = "Utilizador")]
    public class HashtagsController : Controller
    {
        private DatumDbContext db = new DatumDbContext();

        // GET: Hashtags
        [AllowAnonymous]
        public ActionResult Index()
        {
            var hashtags = db.Hashtags.Include(h => h.POI);
            return View(hashtags.OrderBy(a => a.POI.Name).ToList());
        }

        // GET: Hashtags/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hashtag hashtag = db.Hashtags.Find(id);
            if (hashtag == null)
            {
                return HttpNotFound();
            }
            return View(hashtag);
        }

        // GET: Hashtags/Create
        [Authorize(Roles = "Utilizador")]
        public ActionResult Create()
        {
            ViewBag.POIID = new SelectList(db.POIs, "POIID", "Name");
            return View();
        }

        // POST: Hashtags/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Utilizador")]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "HashtagID,Descricao,POIID")] Hashtag hashtag)
        {
            hashtag.UserId = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                db.Hashtags.Add(hashtag);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.POIID = new SelectList(db.POIs, "POIID", "Name", hashtag.POIID);
            return View(hashtag);
        }

        // GET: Hashtags/Edit/5
        [Authorize(Roles = "Utilizador")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hashtag hashtag = db.Hashtags.Find(id);
            if (hashtag == null)
            {
                return HttpNotFound();
            }
            if (hashtag.CheckNotOwner(User.Identity.GetUserId()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            ViewBag.POIID = new SelectList(db.POIs, "POIID", "Name", hashtag.POIID);
            return View(hashtag);
        }

        // POST: Hashtags/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Utilizador")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "HashtagID,Descricao,POIID")] Hashtag hashtag)
        {
            if (ModelState.IsValid)
            {
                Hashtag htag = db.Hashtags.Find(hashtag.HashtagID);
                db.Entry(htag).State = EntityState.Detached;
                hashtag.UserId = User.Identity.GetUserId();
                db.Entry(hashtag).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.POIID = new SelectList(db.POIs, "POIID", "Name", hashtag.POIID);
            return View(hashtag);
        }

        // GET: Hashtags/Delete/5
        [Authorize(Roles = "Utilizador")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hashtag hashtag = db.Hashtags.Find(id);
            if (hashtag == null)
            {
                return HttpNotFound();
            }
            if (hashtag.CheckNotOwner(User.Identity.GetUserId()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }
            return View(hashtag);
        }

        // POST: Hashtags/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Utilizador")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Hashtag hashtag = db.Hashtags.Find(id);
            db.Hashtags.Remove(hashtag);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
