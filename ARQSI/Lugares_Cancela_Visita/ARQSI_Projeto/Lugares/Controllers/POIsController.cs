﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;
using Microsoft.AspNet.Identity;
using System.Diagnostics;
using DataAccessLibrary.Infrastructure;

namespace NewNewLugares.Controllers
{
    [Authorize(Roles = "Administrador, Utilizador")]
    public class POIsController : Controller
    {
        private DatumDbContext db = new DatumDbContext();

        // GET: POIs
        [AllowAnonymous]
        public ActionResult Index()
        {
            var pOIs = db.POIs.Include(p => p.Local).Include(p => p.User).Include(p => p.Hashtags);
            List<POI> validated = new List<POI>();
            foreach(POI poi in pOIs)
            {
                if(poi.Validated)
                {   
                    validated.Add(poi);
                }
            }
            return View(validated);
        }

        // GET: POIs/Details/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POI pOI = db.POIs.Find(id);
            if (pOI == null)
            {
                return HttpNotFound();
            }
            return View(pOI);
        }

        // GET: POIs/Create
        [Authorize(Roles = "Administrador, Utilizador")]
        public ActionResult Create()
        {
            ViewBag.LocalID = new SelectList(db.Locals, "LocalID", "Name");
            ViewBag.UserId = new SelectList(db.Users, "Id", "Email");
            return View();
        }

        // POST: POIs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador, Utilizador")]
        public ActionResult Create([Bind(Include = "POIID,Name,Description,LocalID,UserId,Validated")] POI pOI, string hashTags)
        {
            pOI.UserId = User.Identity.GetUserId();
            if(User.IsInRole("Administrador"))
            {
                pOI.Validated = true;
            }
            if (User.IsInRole("Utilizador"))
            {
                String[] htags = hashTags.Split(',');
                foreach(string hashTg in htags)
                {
                    Hashtag h = new Hashtag();
                    h.Descricao = hashTg;
                    db.Hashtags.Add(h);
                    
                }
            }
            if (ModelState.IsValid)
            {
                db.POIs.Add(pOI);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LocalID = new SelectList(db.Locals, "LocalID", "Name", pOI.LocalID);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Email", pOI.UserId);
            return View(pOI);
        }

        // GET: POIs/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POI pOI = db.POIs.Find(id);
            if (pOI == null)
            {
                return HttpNotFound();
            }

            if (pOI.CheckNotOwner(User.Identity.GetUserId()))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }

            ViewBag.LocalID = new SelectList(db.Locals, "LocalID", "Name", pOI.LocalID);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Email", pOI.UserId);
            return View(pOI);
        }

        // POST: POIs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit([Bind(Include = "POIID,Name,Description,LocalID,UserId,Validated")] POI pOI)
        {
            if (ModelState.IsValid)
            {
                POI poiTest = db.POIs.Find(pOI.POIID);
                if (poiTest.CheckNotOwner(User.Identity.GetUserId()))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                }
                db.Entry(poiTest).State = EntityState.Detached;

                pOI.UserId = User.Identity.GetUserId();
                db.Entry(pOI).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LocalID = new SelectList(db.Locals, "LocalID", "Name", pOI.LocalID);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Email", pOI.UserId);
            return View(pOI);
        }

        // GET: POIs/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POI pOI = db.POIs.Find(id);
            if (pOI == null)
            {
                return HttpNotFound();
            }
            
            return View(pOI);
        }

        // POST: POIs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrador")]
        public ActionResult DeleteConfirmed(int id)
        {
            POI pOI = db.POIs.Find(id);
            db.POIs.Remove(pOI);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult IndexValidated()
        {
            var pOIs = db.POIs.Include(p => p.Local).Include(p => p.User);
            List<POI> validate = new List<POI>();
            foreach (POI poi in pOIs)
            {
                if (!poi.Validated)
                {
                    validate.Add(poi);
                }
            }
            return View(validate);
        }

        [Authorize(Roles ="Administrador")]
        public ActionResult Validate(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            POI pOI = db.POIs.Find(id);
            if (pOI == null)
            {
                return HttpNotFound();
            } else
            {
                pOI.Validated = true;
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }
    }
}
