﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NewNewLugares.Startup))]
namespace NewNewLugares
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
