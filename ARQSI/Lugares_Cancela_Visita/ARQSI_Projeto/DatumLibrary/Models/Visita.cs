﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Models
{
    public enum Day
    {
        Segunda,
        Terça,
        Quarta,
        Quinta,
        Sexta,
        Sábado,
        Domingo
    }
    public class Visita
    {
        public int VisitaID { get; set; }
        [Display(Name = "Dia")]
        public Day Day { get; set; }
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:hh:mm}", ApplyFormatInEditMode = true)]
        [Display(Name = "Hora de Início")]
        public string horaInicio { get; set; }

        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:hh:mm}", ApplyFormatInEditMode = true)]
        [Display(Name = "Hora de Fim")]
        public string horaFim { get; set; }
        public List<POI> pois { get; set; }
    }
}
