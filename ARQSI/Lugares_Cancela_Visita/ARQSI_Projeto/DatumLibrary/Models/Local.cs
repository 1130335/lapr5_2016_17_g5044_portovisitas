﻿using DataAccessLibrary.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Models
{
    public class Local
    {
        public int LocalID { get; set; }
        [Required(ErrorMessage = "No Name was chosen.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "No Latitude value was chosen."), Display(Name = "Latitude"), Range(-90, 90)]
        public float GPS_Lat { get; set; }
        [Required(ErrorMessage = "No Longitude was chosen."), Display(Name = "Longitude"), Range(-180, 180)]
        public float GPS_Long { get; set; }
    }
}
