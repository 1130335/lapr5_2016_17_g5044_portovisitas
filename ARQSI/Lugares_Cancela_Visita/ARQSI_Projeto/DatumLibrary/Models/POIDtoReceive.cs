﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Models
{
    public class POIDtoReceive : PoiDtoAbstract
    {
        [ForeignKey("Local")]
        public int LocalID { get; set; }

        public POIDtoReceive(){ }
        public POIDtoReceive(POI poi)
        {
            this.POIID = poi.POIID;
            this.Name = poi.Name;
            this.Description = poi.Description;
            this.LocalID = poi.LocalID;
        }
    }

}
