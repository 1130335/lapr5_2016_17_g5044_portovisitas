﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Models
{
    public class PoiDtoSend : PoiDtoAbstract 
    {
        public virtual Local Local { get; set; }
        public string Username;

        public PoiDtoSend() { }
        public PoiDtoSend(POI poi)
        {
            this.POIID = poi.POIID;
            this.Name = poi.Name;
            this.Description = poi.Description;
            this.Local = poi.Local;
            this.Username = poi.User.UserName;
        }
    }
}
