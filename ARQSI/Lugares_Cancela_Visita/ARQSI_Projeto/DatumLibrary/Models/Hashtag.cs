﻿using DataAccessLibrary.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Models
{
    public class Hashtag
    {
        public int HashtagID { get; set; }
        public string Descricao { get; set; }
        public int POIID { get; set; }
        public virtual POI POI { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        [Editable(false)]
        public virtual ApplicationUser User { get; set; }

        public bool CheckNotOwner(string userId)
        {
            return userId != this.UserId;
        }
    }
}
