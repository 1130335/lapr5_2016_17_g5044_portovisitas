﻿using DataAccessLibrary.DAL;
using DataAccessLibrary.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.Models
{
    public class POI
    {
        public int POIID { get; set; }
        [Required(ErrorMessage = "No Name was chosen.")]
        public string Name { get; set; }
        public string Description { get; set; }
        [ForeignKey("Local")]
        public int LocalID { get; set; }
        public virtual Local Local { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        [Editable(false)]
        public virtual ApplicationUser User { get; set; }
        public bool Validated { get; set; }
        public bool Checked { get; set; }
        public virtual ICollection<Hashtag> Hashtags { get; set; }

        public POI() { }
        public POI(POIDtoReceive dto, string userId)
        {
            this.POIID = dto.POIID;
            this.Name = dto.Name;
            this.Description = dto.Description;
            this.LocalID = dto.LocalID;
            this.UserId = userId;

            DatumDbContext db = new DatumDbContext();
            this.User = db.Users.Find(this.UserId);
            this.Local = db.Locals.Find(this.LocalID);
        }



        public bool CheckNotOwner(string userId)
        {
            return userId != this.UserId;
        }
    }
}
