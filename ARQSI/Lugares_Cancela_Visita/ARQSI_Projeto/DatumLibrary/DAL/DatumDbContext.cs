﻿using DataAccessLibrary.Infrastructure;
using DataAccessLibrary.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DataAccessLibrary.Infrastructure.ApplicationUser;

namespace DataAccessLibrary.DAL
{
    public class DatumDbContext : IdentityDbContext<ApplicationUser>
    {
        public DatumDbContext() : base("Datum") {
        }
        public DbSet<POI> POIs { get; set; }
        public DbSet<Local> Locals { get; set; }
        public DbSet<Meteorology> Meteorologias { get; set; }
        public DbSet<Hashtag> Hashtags { get; set; }

        public static DatumDbContext Create()
        {
            return new DatumDbContext();
        }

        public System.Data.Entity.DbSet<DataAccessLibrary.Models.Visita> Visitas { get; set; }
    }
}
