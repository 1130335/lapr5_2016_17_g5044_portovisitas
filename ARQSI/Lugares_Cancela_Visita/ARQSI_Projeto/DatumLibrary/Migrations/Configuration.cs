namespace DataAccessLibrary.Migrations
{
    using Infrastructure;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DataAccessLibrary.DAL.DatumDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(DataAccessLibrary.DAL.DatumDbContext context)
        {
            //Roles
            if (!context.Roles.Any(r => r.Name == "Administrador"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Administrador" };

                manager.Create(role);
            }
            if (!context.Roles.Any(r => r.Name == "Utilizador"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Utilizador" };

                manager.Create(role);
            }

            //Users
            if (!context.Users.Any(u => u.UserName == "founder"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = "founder", Email = "founder@founder.com"};

                manager.Create(user, "ChangeItAsap!");
                manager.AddToRole(user.Id, "Administrador");
                manager.AddToRole(user.Id, "Utilizador");
            }
            if (!context.Users.Any(u => u.UserName == "founder2"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = "founder2", Email = "founder2@founder.com" };

                manager.Create(user, "ChangeItAsap!");
                manager.AddToRole(user.Id, "Administrador");
                manager.AddToRole(user.Id, "Utilizador");
            }
            if (!context.Users.Any(u => u.UserName == "widget"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = "widget@widget.com", Email = "widget@widget.com" };

                manager.Create(user, "widgetPassword2016!");
                manager.AddToRole(user.Id, "Utilizador");
                manager.AddToRole(user.Id, "Administrador");
            }

            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            var xuser = userManager.FindByName("founder");
            var xuser2 = userManager.FindByName("founder2");
            context.Locals.AddOrUpdate(x => x.LocalID,
                new Local() { LocalID = 1, Name = "Local 1", GPS_Lat = -90, GPS_Long = -120 },
                new Local() { LocalID = 2, Name = "Local 2", GPS_Lat = 0, GPS_Long = 180 },
                new Local() { LocalID = 3, Name = "Local 3", GPS_Lat = 90, GPS_Long = 90 });
            context.POIs.AddOrUpdate(x => x.POIID,
                new POI()
                {
                    POIID = 1,
                    Name = "Name 1",
                    Description = "Des1",
                    LocalID = 1,
                    UserId = xuser.Id,
                    Validated = true
                },
                 new POI()
                 {
                     POIID = 2,
                     Name = "Name 2",
                     Description = "Des2",
                     LocalID = 1,
                     UserId = xuser.Id,
                     Validated = true
                 },
                  new POI()
                  {
                      POIID = 3,
                      Name = "Name 3",
                      Description = "Des3",
                      LocalID = 2,
                      UserId = xuser2.Id,
                      Validated = true
                  }
                );

            context.Meteorologias.AddOrUpdate(x => x.MeteorologyID,
                new Meteorology()
                {
                    MeteorologyID = 3,
                    LocalID = 2,
                    TimeOfReading = new DateTime(2016, 5, 2),
                    HourOfReading = new DateTime(2016, 5, 2, 22, 44, 0),
                    Temperature = 2,
                    Wind = 5,
                    Humidity = 7,
                    Pressure = 8,
                    NO = 9,
                    NO2 = 10,
                    CO2 = 100,
                });

            //Used to set username the same as email. Can't be done before because ???????????
            xuser.UserName = "founder@founder.com";
            xuser2.UserName = "founder2@founder.com";
        }
    }
}
