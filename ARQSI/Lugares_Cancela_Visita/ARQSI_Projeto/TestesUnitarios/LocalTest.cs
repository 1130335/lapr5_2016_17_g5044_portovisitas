﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataAccessLibrary.Models;
using System.Collections.Generic;
using Lugares.Controllers;
using System.Threading.Tasks;

namespace TestesUnitarios
{
    [TestClass]
    public class LocalTest
    {
        [TestMethod]
        public void GetAllLocals_ShouldReturnAllLocals()
        {
            var testLocals = GetTestLocals();
            var controller = new LocalsController();

            var result = controller.GetAllLocals() as List<Local>;
            Assert.AreEqual(testLocals.Count, result.Count);
        }

        [TestMethod]
        public async Task GetAllLocalsAsync_ShouldReturnAllLocals()
        {
            var testLocals = GetTestLocals();
            var controller = new LocalsController();

            var result = await controller.GetAllLocalsAsync() as List<Local>;
            Assert.AreEqual(testLocals.Count, result.Count);
        }

        [TestMethod]
        public void GetLocal_ShouldReturnCorrectLocals()
        {
            var testLocals = GetTestLocals();
            var controller = new LocalsController();

            var result = controller.GetLocal(2) as OkNegotiatedContentResult<Local>;
            Assert.IsNotNull(result);
            Assert.AreEqual(testLocals[1].Name, result.Content.Name);
        }

        [TestMethod]
        public async Task GetLocalsAsync_ShouldReturnCorrectLocal()
        {
            var testLocals = GetTestLocals();
            var controller = new LocalsController();

            var result = await controller.GetLocalAsync(2) as OkNegotiatedContentResult<Local>;
            Assert.IsNotNull(result);
            Assert.AreEqual(testLocals[1].Name, result.Content.Name);
        }

        [TestMethod]
        public void GetLocals_ShouldNotFindLocals()
        {
            var controller = new LocalsController();

            var result = controller.GetLocal(999);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        private List<Local> GetTestLocals()
        {
            var testLocals = new List<Local>();
            testLocals.Add(new Local { LocalID = 1, Name = "Demo1", GPS_Lat = 1, GPS_Long = 3 });
            testLocals.Add(new Local { LocalID = 2, Name = "Demo2", GPS_Lat = 2, GPS_Long = 5 });
            return testLocals;
        }
    }
}
