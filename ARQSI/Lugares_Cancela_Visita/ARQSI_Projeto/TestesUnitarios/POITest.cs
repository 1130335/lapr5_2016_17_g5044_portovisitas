﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataAccessLibrary.Models;
using System.Collections.Generic;
using Visita.Controllers;
using System.Threading.Tasks;

namespace TestesUnitarios
{
    [TestClass]
    public class POITest
    {
        [TestMethod]
        public void GetAllPOIs_ShouldReturnAllPOIs()
        {
            var testPOIs = GetTestPOIs();
            var controller = new POIsController();

            var result = controller.GetAllPOIs() as List<POI>;
            Assert.AreEqual(testPOIs.Count, result.Count);
        }

        [TestMethod]
        public async Task GetAllPOIsAsync_ShouldReturnAllPOIs()
        {
            var testPOIs = GetTestPOIs();
            var controller = new POIsController();

            var result = await controller.GetAllPOIsAsync() as List<POI>;
            Assert.AreEqual(testPOIs.Count, result.Count);
        }

        [TestMethod]
        public void GetPOI_ShouldReturnCorrectPOIs()
        {
            var testPOIs = GetTestPOIs();
            var controller = new POIsController();

            var result = controller.GetPOI(2) as OkNegotiatedContentResult<POI>;
            Assert.IsNotNull(result);
            Assert.AreEqual(testPOIs[1].Name, result.Content.Name);
        }

        [TestMethod]
        public async Task GetPOIAsync_ShouldReturnCorrectPOI()
        {
            var testPOIs = GetTestPOIs();
            var controller = new POIsController();

            var result = await controller.GetPOIAsync(2) as OkNegotiatedContentResult<POI>;
            Assert.IsNotNull(result);
            Assert.AreEqual(testPOIs[1].Name, result.Content.Name);
        }

        [TestMethod]
        public void GetPOIs_ShouldNotFindPOIs()
        {
            var controller = new POIsController();

            var result = controller.GetPOI(999);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        private List<POI> GetTestPOIs()
        {
            var testPOIs = new List<POI>();
            testPOIs.Add(new POI { POIID = 1, Description = "P1", LocalID = 1, UserId = "1", Name = "POI1" });
            testPOIs.Add(new POI { POIID = 2, Description = "P2", LocalID = 2, UserId = "2", Name = "POI2" });
            return testPOIs;
        }
    }
}


