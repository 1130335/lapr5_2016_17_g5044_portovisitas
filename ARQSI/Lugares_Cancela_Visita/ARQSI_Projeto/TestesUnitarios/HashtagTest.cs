﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataAccessLibrary.Models;
using NewNewLugares.Controllers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TestesUnitarios
{
    [TestClass]
    public class HashtagTest
    {
        [TestMethod]
        public void GetAllHashtags_ShouldReturnAllHashtag()
        {
            var testHashtags = GetTestHashtags();
            var controller = new HashtagsController();

            var result = controller.GetAllHashtags() as List<Hashtag>;
            Assert.AreEqual(testHashtags.Count, result.Count);
        }

        [TestMethod]
        public async Task GetAllProductsAsync_ShouldReturnAllProducts()
        {
            var testHashtags = GetTestHashtags();
            var controller = new HashtagsController();

            var result = await controller.GetAllHashtagsAsync() as List<Hashtag>;
            Assert.AreEqual(testHashtags.Count, result.Count);
        }

        [TestMethod]
        public void GetHashtag_ShouldReturnCorrectHashtag()
        {
            var testHashtags = GetTestHashtags();
            var controller = new HashtagsController();

            var result = controller.GetHashtag(2) as OkNegotiatedContentResult<Hashtag>;
            Assert.IsNotNull(result);
            Assert.AreEqual(testHashtags[1].Descricao, result.Content.Descricao);
        }

        [TestMethod]
        public async Task GetHashtagAsync_ShouldReturnCorrectHashtag()
        {
            var testHashtags = GetTestHashtags();
            var controller = new HashtagsController();

            var result = await controller.GetHashtagAsync(2) as OkNegotiatedContentResult<Hashtag>;
            Assert.IsNotNull(result);
            Assert.AreEqual(testHashtags[1].Descricao, result.Content.Descricao);
        }

        [TestMethod]
        public void GetHashtag_ShouldNotFindHashtag()
        {
            var controller = new HashtagsController();

            var result = controller.GetHashtag(999);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        private List<Hashtag> GetTestHashtags()
        {
            var testHashtags = new List<Hashtag>();
            testHashtags.Add(new Hashtag { HashtagID = 1, Descricao = "H1", POIID = 1, UserId = "1" });
            testHashtags.Add(new Hashtag { HashtagID = 2, Descricao = "H2", POIID = 2, UserId = "2" });

            return testHashtags;
        }
    }
}
